\documentclass[12pt,letterpaper]{article}

%hyphenation
\usepackage[spanish]{babel}	

\usepackage{array}	

% hacer que un elemeno ocupe múltiples filas
\usepackage{multirow}


\usepackage{vwcol}

\usepackage{wrapfig}

%caracteres especiales
%COMENTAR SI SE USA LUATEX, XETEX
%\usepackage[utf8]{inputenc}		

%random text
\usepackage{lipsum}
\usepackage{blindtext}

%recuadro
% Rellenar una celda de tabla con un color dado
\usepackage[usenames, table]{xcolor}
\usepackage[framemethod=TikZ]{mdframed}

% color números
%	\setbeamercolor{local structure}{fg=darkred}

%\usepackage{etoolbox}
%\usepackage{tikz}
%\usepackage{unicode-math}
%\setsansfont[Scale=MatchUppercase]{Candara}

%%%%% Definición de tipo de fuente 

% % http://www.tug.dk/FontCatalogue/
%\usepackage[T1]{fontenc}
%\usepackage{candara}

%\usepackage[default]{uni}
%\usepackage[OT1]{fontenc} %% Universal doesn't work with T1


\usepackage{fontspec}
\setmainfont[Ligatures={Common,TeX}, Numbers={OldStyle}]{Amiri}

%\renewcommand*{\familydefault}{\sfdefault}
%\setsansfont[Scale=MatchUppercase]{Candara}


%\usepackage[urw-garamond]{mathdesign}
%\renewcommand{\sfdefault}{qag}

%uso de múltiples columnas en seccionees definidas
%http://bioinformatiquillo.wordpress.com/2009/02/16/lyx-latex-columnas-con-el-paquete-multicol/
\usepackage{multicol}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

% adding postscript
\usepackage{graphicx,epstopdf}
\epstopdfsetup{update}
\DeclareGraphicsExtensions{.ps}

% Encabezado
\usepackage{fancyhdr}

%definición de colores
%\usepackage[usenames, dvipsnames]{color}
\definecolor{rojo}{RGB}{185, 42, 41}
\definecolor{azul}{RGB}{42, 74, 136}
\definecolor{negro}{RGB}{40,40,40}
\definecolor{cafe2}{RGB}{137,112,67}
\definecolor{rojo2}{RGB}{170,63,60}
\definecolor{naranja}{RGB}{228,108,10}
\definecolor{amarillo}{RGB}{240,234,0}
\definecolor{verde}{RGB}{119,147,60}
\definecolor{azul2}{RGB}{55,96,146}
\definecolor{violeta}{RGB}{96,74,123}
\definecolor{gris}{RGB}{166,166,166}
\definecolor{blanco}{RGB}{255,255,255}
\definecolor{dorado}{RGB}{219, 204, 41}
\definecolor{plata}{RGB}{219,204,219}

%redefinición de secciones
\usepackage{titlesec}

\titleformat{\section}
  {\color{azul}\normalfont\Large\bfseries}{\thesection}{1em}{}[{\titlerule[0.7pt]}]
  
\titleformat{\subsection}
  {\color{rojo}\normalfont\large\bfseries}{\thesubsection}{1em}{}[{{\titlerule[0.7pt]}}]
  
\titleformat{\subsubsection}
  {\color{azul}\normalfont\normalsize\bfseries\itshape}{\thesubsubsection}{1em}{}[{}]

%\titleformat{\subsection}
%  {\color{MetallicGold}\normalfont\Large\bfseries}{\thesubsection}{1em}{}[{\titlerule[0.8pt]}]




\usepackage{caption}
\usepackage{float}
\usepackage{moreverb}	%verbatimtab

\usepackage{listings}	% syntax highlighting
\usepackage{color}
\usepackage{showexpl}

\usepackage{url}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstdefinestyle{st1}{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\scriptsize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={},            % if you want to delete keywords from the given language
  escapeinside={},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=java,                 % the language of the code
  morekeywords={},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style  tabsize=3%,                       % sets default tabsize to 2 spaces
  %title={}                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\lstdefinestyle{st2}{
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\scriptsize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={},            % if you want to delete keywords from the given language
  escapeinside={},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=java,                 % the language of the code
  morekeywords={},            % if you want to add more keywords to the set
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  explpreset={numbers=none},  
  tabsize=3%,                       % sets default tabsize to 2 spaces
  %title={}                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\lstset{style=st2}
\lstset{inputencoding=ansinew}


%margenes http://en.wikibooks.org/wiki/LaTeX/Page_Layout
%Margen superior de las páginas con encabezado
\usepackage{titling}
\setlength{\droptitle}{-0.8 in}

\voffset -10 mm

\topmargin    0 mm	% ~3 cm margen superior
\headheight   12pt

\headsep 15 mm

\textheight       210mm  
\textwidth        160mm   

\oddsidemargin        30 mm	%~2.4 cm margen izq
\evensidemargin       30 mm

\addtolength{\oddsidemargin}{-1in}
\addtolength{\evensidemargin}{-1in}


% The distance from the lower edge of the text body to the
% footline
\footskip 0.4in

% Interlineado
\renewcommand{\baselinestretch}{1.0}

% Espacio entra párrafos, relativo al interlineado
\setlength{\parskip}{0.8\baselineskip}



\parindent    0.0em


% Definición de estilo de página, encabezado y pie
\pagestyle{fancy}
\fancyhf{}
\lhead{\textcolor{azul}{\footnotesize Led RGB}}
\rhead{\textcolor{azul}{\footnotesize Kevin Patiño y Ana Moreno}}
%\rfoot{Página \thepage}

\newmdtheoremenv
[ %
outerlinewidth = 1 ,
roundcorner = 10 pt ,
leftmargin = 40 ,
rightmargin = 40 ,
backgroundcolor = white ,
outerlinecolor = blue,
innertopmargin = \topskip,
splittopskip = \topskip,
ntheorem = false ,
]{recuadro}{}[section]

\newenvironment{myenv1}[1]
  {\mdfsetup{
    frametitle={\color{rojo}\colorbox{white}{\space#1\space}},
    innertopmargin=10pt,
    frametitleaboveskip=-\ht\strutbox,
    frametitlealignment=\hspace*{0.0\linewidth},
    leftmargin = 10 ,
	rightmargin = 10 ,
	 middlelinecolor=rojo,
	 roundcorner=0pt
    }
  \begin{mdframed}%
  }
  {\end{mdframed}}
  
  \newenvironment{myenv2}[1]
  {\mdfsetup{
    frametitle={\color{azul}\colorbox{white}{\space#1\space}},
    innertopmargin=10pt,
    frametitleaboveskip=-\ht\strutbox,
    frametitlealignment=\hspace*{0.85\linewidth},
    leftmargin = 10 ,
	rightmargin = 10 ,
	 middlelinecolor=azul,
	 roundcorner=0pt
    }
  \begin{mdframed}%
  }
  {\end{mdframed}}
  
  
  
  \newenvironment{eq-frame}[1]
  {\mdfsetup{
    frametitle={},
    innertopmargin=10pt,
    frametitleaboveskip=-\ht\strutbox,
    frametitlealignment=\hspace*{0.85\linewidth},
    leftmargin = 120 ,
	rightmargin = 120 ,
	 middlelinecolor=azul,
	 roundcorner=1pt
    }
  \begin{mdframed}%
  }
  {\end{mdframed}}





%%%%%%%%%%%%%%%% begin document %%%%%%%%%%%%%%%%%%%%
\begin{document} 
\title{\textcolor{rojo}{\bf Led RGB}} 
\renewcommand{\baselinestretch}{0.5}
%Espacio entre el encabezado y el cuerpo.
%Con \headsep no se obtuvo el efecto deseado
\date{\vspace{-14ex}}
\author{\vspace{-15ex}}
%

%Elimina números de página. Hace aparecer el título
%\pagestyle{empty}
\maketitle

%\thispagestyle{empty}
\thispagestyle{fancy}



%%%%%%%%%%%%%%% HERE BEGINS THE BODY %%%%%%%%%%%%%%%
% Interlineado

\begin{center}
\normalsize Universidad Nacional de Colombia -- Sede Bogotá \\
\end{center}

\renewcommand{\baselinestretch}{1.3}
\selectfont

\begin{figure}[H]
  \centering
    \includegraphics[width=0.25\textwidth]{../pics/led_rgb.png}
  \caption{led RGB}
  \label{sensr}
\end{figure}

\section*{LED (Light-emitting diode)}
Un led (diodo emisor de luz) funciona como un diodo común ya que al estar en polarización directa permite que la corriente fluya con la única diferencia  de que al hacerlo emite luz, ésta varía su color de acuerdo al material semiconductor que constitutye al componente. 
 
\section*{Parte 1: Led RGB  }

Un led RGB está compuesto por tres leds, éstos son de color rojo, verde y azul. Tiene cuatro pines para controlar su funcionamiento, el pin \textbf{--} debe ir conectado a tierra y los demás \textbf{R,G,B} al conectarlos a una tensión determinada emitirán la luz que indica su pin, siendo \textbf{R} rojo, \textbf{B} azul, \textbf y {G} verde.



\mdfsetup{%
	leftmargin=40,
	rightmargin=40,
	innertopmargin=20,
	innerbottommargin=20,
	innerleftmargin=12,
	innerrightmargin=20,
   middlelinecolor=azul,
   middlelinewidth=1pt,
   backgroundcolor=white,
   roundcorner=2pt}

\begin{figure}[H]
  \centering
    \includegraphics[width=0.3\textwidth]{../pics/led_rgb1.png}
  \caption{Conexión interna del led RGB}
  \label{sensr1}
\end{figure}
\bigskip
\bigskip





\nopagebreak
\begin{myenv1}{Ejemplo: Colores:Rojo, Azul y Verde }

Utilizando IFLAB y el led RGB

\begin{itemize}
\item Conexiones

En este ejemplo se conecta la tierra del led a la tierra de la tarjeta y cada salida digital \textbf{LED3}, \textbf{LED2}, \textbf{LED1}  a una resistencia en serie al pin de cada color del led.La resistencia se conecta con el fin de limitar que pasa por el led y evitar que éste se queme.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.9\textwidth]{../pics/led_rgb2.png}
  \caption{Conexiones del led RGB a IFLAB}
  \label{sensr2}
\end{figure}

\item Programación en Bloques
Para hacer el programa de un medidor de distacia básico, se debe trabajar en el entorno de desarrollo, hacer el  programa de la figura\ref{progsensr1} y correrlo .
\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{../pics/colores.png}
  \caption{Colores rojo,azul y verde}
  \label{progsensr1}
\end{figure}
\end{itemize}

\begin{multicols}{2}
\begin{itemize}

\end{itemize}
\end{multicols}


\end{myenv1}

\newpage


\begin{myenv1}{Ejemplo: Colores obtenidos a partir del rojo, azul y verde}

Utilizando IFLAB y el led RGB

\begin{itemize}
\item Conexiones
Realizando las mismas conexiones mostradas en la figura \ref{sensr2}

\item Programación en Bloques
Para hacer el programa de un medidor de distacia básico, se debe trabajar en el entorno de desarrollo, hacer el  programa de la figura\ref{progsensr} y correrlo .
\begin{figure}[H]
  \centering
  \includegraphics[width=0.4\textwidth]{../pics/combinaciones.png}
  \caption{Colores obtenidos a partir del rojo, azul y verde }
  \label{progsensr}
\end{figure}
\end{itemize}

\begin{multicols}{2}
\begin{itemize}

\end{itemize}
\end{multicols}




\end{myenv1}

\newpage



\end{document}
