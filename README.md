# IFLAB - Computador de Laboratorio #

### Introducción ###

El uso de estrategias pedagógicas innovadoras ha dado paso a la construcción de plataformas eletrónicas educativas abiertas, que facilitan  la interacción de los estudiantes con los nuevos retos existentes en el ámbito tecnológico. Por lo anterior se  desarrolló **IFLAB-Computador de Laboratorio**, como una herramienta que permite al estudiante afianzar y adquirir habilidades en diferentes áreas de conocimiento; presenta una interfaz gráfica basada en un lenguaje de programación por bloques, permitiendo que el estudiante pueda concentrar su atención en el algoritmo y no en la sintaxis del código. 
